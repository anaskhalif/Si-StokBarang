<?php 

require 'function.php';

// mendapatakan Id
$idbarang = $_GET['id'];

// ambil data dari database
$get = mysqli_query($conn, "SELECT * FROM stock WHERE idbarang = '$idbarang'");
$fetch = mysqli_fetch_array($get);

// set variabel
$namabarang = $fetch["namabarang"];
$deskripsi = $fetch["deskripsi"];
$stock = $fetch["stock"];
$image = $fetch["image"];

// ada gambar atau tidak
$gambar = $fetch["image"]; // ambil gambar
if( $gambar == null ) {
// jika tidak ada gambar
    $img = "No Photo";
} else {
// jika ada gambar
    $img = '<img class="card-img-top" src="img/'.$gambar.'" alt="Card image" style="width:100%">';
}


 ?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
	<title>Menampilkan Barang</title>
</head>
<body>
	
	<div class="container">
		<h3>Detail Barang</h3>
  <div class="card mt-4" style="width:400px">
    <?= $img; ?>
    <div class="card-body">
      <h4 class="card-title"><?= $namabarang; ?></h4>
      <h4 class="card-text"><?= $deskripsi; ?></h4>
      <h4 class="card-text"><?= $stock; ?></h4>
    </div>
  </div>
  <br>

	</div>

</body>
</html>
